def on_on_event():
    radio.send_value("pitch", input.rotation(Rotation.PITCH))
    basic.pause(1)
    radio.send_value("roll", input.rotation(Rotation.ROLL))
    basic.pause(1)
    radio.send_value("yaw", input.compass_heading())
    basic.pause(10)
    control.raise_event(4800, 4801)
control.on_event(4800, 4801, on_on_event)

led.set_brightness(90)
radio.set_group(18)
control.raise_event(4800, 4801)

def on_every_interval():
    basic.show_leds("""
        . . . . .
                . . . . .
                . . # . .
                . . . . .
                . . . . .
    """)
    basic.pause(5)
    basic.clear_screen()
loops.every_interval(1000, on_every_interval)
